package client;

import static org.kie.dmn.core.util.DynamicTypeUtils.entry;
import static org.kie.dmn.core.util.DynamicTypeUtils.prototype;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.demo.SuspiciousActivity;
import com.demo.Transaction;

import org.kie.dmn.api.core.DMNContext;
import org.kie.dmn.api.core.DMNDecisionResult;
import org.kie.dmn.api.core.DMNResult;
import org.kie.server.api.marshalling.MarshallingFormat;
import org.kie.server.api.model.ServiceResponse;
import org.kie.server.api.model.cases.CaseFile;
import org.kie.server.client.CaseServicesClient;
import org.kie.server.client.DMNServicesClient;
import org.kie.server.client.KieServicesClient;
import org.kie.server.client.KieServicesConfiguration;
import org.kie.server.client.KieServicesFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;;

public class Main {

	final static Logger log = LoggerFactory.getLogger(Main.class);

	private static final String URL = "http://localhost:8080/kie-server/services/rest/server";
	private static final String user = System.getProperty("username", "donato");
	private static final String password = System.getProperty("password", "donato");
	private static final String CONTAINER = "proactive-fraud-detection-case_1.0.0-SNAPSHOT";
	private static String PROCESS_ID = "proactivefrauddetectioncase.fraud-case";
	private static String DMN_CONTAINER = "proactive-fraud-detection-dmn_1.0.0-SNAPSHOT";

	public static void main(String[] args) {
		System.setProperty("org.kie.server.json.format.date", "true");

		Main clientApp = new Main();
		long start = System.currentTimeMillis();

		clientApp.launchCase();
		//clientApp.evaluateDMN();

		long end = System.currentTimeMillis();

		log.info("elapsed time: {}", (end - start));
	}

	private void evaluateDMN() {
		try {
			String modelNamespace = "http://www.redhat.com/dmn/definitions/_81556584-7d78-4f8c-9d5f-b3cddb9b5c73";
			String modelName = "fraud-scoring";

			KieServicesClient client = getClient();

			DMNServicesClient dmnClient = client.getServicesClient(DMNServicesClient.class);

			DMNContext dmnContext = dmnClient.newContext();
			//[{"Auth Code":"Authorized", "Amount":"100", "Merchant Code":"LEGITBIZ", "Card Type":"Debit", "Date":"date and time(\"2020-02-14T12:45:56\")", "Location":"Local"},
			// {"Auth Code":"Autherized", "Amount":"200", "Merchant Code":"LEGITBIZ", "Card Type":"Debit", "Date":"date and time(\"2020-02-15T14:45:56\")", "Location":"Local"},			

			Map<String, Object> transaction1 = prototype(entry("Auth Code", "Authorized"),
														entry("Amount", 100),
														entry("Merchant Code", "LEGITBIZ"),
														entry("Card Type", "Debit"),
														entry("Date", LocalDateTime.now()),
														entry("Location", "Local")
														);

			Map<String, Object> transaction2 = prototype(entry("Auth Code", "Authorized"),
														entry("Amount", 200),
														entry("Merchant Code", "LEGITBIZ"),
														entry("Card Type", "Debit"),
														entry("Date", LocalDateTime.now()),
														entry("Location", "Local")
														);
													
			List<Map<String, Object>> transactions = Arrays.asList(transaction1, transaction2);

			dmnContext.set("Transactions", transactions);
			// kieserver call
			ServiceResponse<DMNResult> serverResp = dmnClient.evaluateAll(DMN_CONTAINER, modelNamespace, modelName,
					dmnContext);

			DMNResult dmnResult = serverResp.getResult();
			for (DMNDecisionResult dr : dmnResult.getDecisionResults()) {
				log.info("Decision: '" + dr.getDecisionName() + "', " + "Result: " + dr.getResult());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void launchCase() {
		try {

			KieServicesClient client = getClient();

			CaseServicesClient processClient = client.getServicesClient(CaseServicesClient.class);
			// CaseFile caseFile = new CaseFile();

			CaseFile caseFile = new CaseFile();
			Map<String, Object> data = new HashMap<>();
			SuspiciousActivity activity = new SuspiciousActivity();
			activity.setRiskRanking(2);
			List<Transaction> transactions = new ArrayList<>(1);

			Transaction transaction = new Transaction();
			transaction.setAmount(250.0);
			transaction.setAuthCode("543");
			transaction.setCardType("secured credit");
			transaction.setDateTimeExec(new Date());
			transaction.setLocation("635 Sutter Street, Union Square, San Francisco, CA 94102, USA");
			transaction.setMerchantCode("456-654-1234");
			transaction.setMerchantName("Hotel BestPlace");

			transactions.add(transaction);

			transaction = new Transaction();
			transaction.setAmount(550.0);
			transaction.setAuthCode("567");
			transaction.setCardType("secured credit");
			transaction.setDateTimeExec(new Date());
			transaction.setLocation("635 Sutter Street, Union Square, San Francisco, CA 94102, USA");
			transaction.setMerchantCode("456-654-1234");
			transaction.setMerchantName("Hotel BestPlace");

			transactions.add(transaction);

			activity.setTransactions(transactions);

			data.put("activity", activity);

			caseFile.setData(data);

			processClient.startCase(CONTAINER, PROCESS_ID, caseFile);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private KieServicesClient getClient() {
		KieServicesConfiguration config = KieServicesFactory.newRestConfiguration(URL, user, password);
		// Marshalling
		config.setMarshallingFormat(MarshallingFormat.JSON);
		Set<Class<?>> extraClasses = new HashSet<Class<?>>();
		extraClasses.add(SuspiciousActivity.class);
		extraClasses.add(Transaction.class);
		config.addExtraClasses(extraClasses);
		Map<String, String> headers = null;
		config.setHeaders(headers);
		KieServicesClient client = KieServicesFactory.newKieServicesClient(config);

		return client;
	}

}
